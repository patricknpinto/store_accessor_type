# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'store_accessor_type/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'store_accessor_type'
  spec.version     = StoreAccessorType::VERSION
  spec.authors     = ['Patrick Pinto']
  spec.email       = ['hello@patricknpinto.com']
  spec.homepage    = 'http://patricknpinto.com'
  spec.summary     = 'Define types to your store_accessor attributes'
  spec.description = 'Define types to your store_accessor attributes'
  spec.license     = 'MIT'
  spec.required_ruby_version = '>= 2.6'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # if spec.respond_to?(:metadata)
  #   spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  # else
  #   raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.'
  # end

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency 'monetize', '~> 1.9'
  spec.add_dependency 'money', '~> 6.13.2'
  spec.add_dependency 'rails', '>= 6.0'

  spec.add_development_dependency 'faker', '~> 2.10'
  spec.add_development_dependency 'pg', '>= 0.18', '< 2.0'
  spec.add_development_dependency 'rspec-rails', '~> 3.9'
  spec.add_development_dependency 'rubocop', '~> 0.79'
  spec.add_development_dependency 'rubocop-rspec', '~> 1.38'
  spec.add_development_dependency 'simplecov', '~> 0.18'

  spec.add_development_dependency 'pry-byebug', '~> 3.8'
  spec.add_development_dependency 'pry-rails', '~> 0.3'
end
