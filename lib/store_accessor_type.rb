# frozen_string_literal: true

require 'store_accessor_type/railtie'

module StoreAccessorType
  extend ActiveSupport::Concern

  TYPES = %w[integer boolean date datetime money enum phone].freeze

  class_methods do
    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/CyclomaticComplexity
    # rubocop:disable Metrics/PerceivedComplexity
    # rubocop:disable Metrics/MethodLength
    def store_accessor_type(*fields)
      options = fields.extract_options!

      type = options[:type].to_s

      raise(ArgumentError, "`#{type}` is not a valid type") unless TYPES.include?(type)

      fields.each do |field|
        case type
        when 'date'
          define_method "#{field}=" do |value|
            super(value.to_date.to_s) if value.respond_to?(:to_date)
          end

          define_method field do
            super()&.to_date
          end
        when 'boolean'
          define_method "#{field}=" do |value|
            super(ActiveModel::Type::Boolean.new.cast(value))
          end

          define_method "#{field}?" do
            send(field)
          end
        when 'money'
          short_name = field.to_s.gsub('_cents', '')

          validate "valid_number_#{short_name}".to_sym

          define_method "valid_number_#{short_name}" do
            return if instance_variable_get("@raw_#{short_name}").blank?

            errors.add(short_name.to_sym, :invalid)
          end

          define_method "#{short_name}=" do |value|
            return if value.blank?

            send("#{field}=", value.to_money.fractional)
          rescue Monetize::ParseError, ArgumentError
            # this runs before validation so if it fails it sets the raw falue to the field
            # and it also sets the `@raw_#{short_name}` instance variable. on validation if that
            # instance variable exists it means that there is a validation error
            send("#{field}=", value)
            instance_variable_set("@raw_#{short_name}", value)
          end

          define_method short_name do
            return nil if send(field).nil?

            Money.new(send(field))
          end

          define_method "#{short_name}_currency" do
            # TODO: read client configuration for default currency
            super() || 'AUD'
          end
        when 'integer'
          define_method "#{field}=" do |value|
            super(value.to_i)
          end
        when 'datetime'
          define_method "#{field}=" do |value|
            super(value.in_time_zone.to_s) if value.respond_to?(:in_time_zone)
          end

          define_method field do
            return nil if super().nil?

            Time.zone.parse(super())
          end
        when 'phone'
          # TODO: make this to work without having to define "#{field}_country" in client
          define_method "#{field}_country" do
            # TODO: read client configuration for default country
            super() || 'AU'
          end
        when 'enum'
          values = options[:values]
          plural_name = field.to_s.pluralize

          define_singleton_method plural_name do
            out = {}
            values.each_with_index { |value, index| out[value.to_s] = index }
            out
          end

          define_method "#{field}=" do |value|
            if value.is_a?(Integer)
              if value >= self.class.send(plural_name).size
                raise(ArgumentError, "'#{field}' is not a valid #{field}")
              end

              super(value)
            else
              int_value = self.class.send(plural_name)[value.to_s]

              raise(ArgumentError, "'#{field}' is not a valid #{field}") if int_value.nil?

              super(int_value)
            end
          end

          define_method field do
            return nil if super().nil?

            self.class.send(plural_name).invert[super()]
          end

          values.each do |value|
            define_method "#{value}?" do
              send(field) == value.to_s
            end

            define_method "#{value}!" do
              update("#{field}": value)
            end
          end
        end
      end
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/CyclomaticComplexity
    # rubocop:enable Metrics/PerceivedComplexity
    # rubocop:enable Metrics/MethodLength
  end
end
