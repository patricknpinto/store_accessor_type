# frozen_string_literal: true

class Item < ApplicationRecord
  include StoreAccessorType

  store_accessor :data, :amount, :confirmed, :paid_at, :submitted_at, :price_cents, :category,
                 :phone, :phone_country

  # TYPES = %w[integer boolean date datetime money enum phone].freeze

  store_accessor_type :amount, type: :integer
  store_accessor_type :confirmed, type: :boolean
  store_accessor_type :paid_at, type: :date
  store_accessor_type :submitted_at, type: :datetime
  store_accessor_type :price_cents, type: :money
  store_accessor_type :category, type: :enum, values: %i[medical income other]
  store_accessor_type :phone, type: :phone
end
