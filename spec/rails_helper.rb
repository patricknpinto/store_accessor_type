# frozen_string_literal: true

# This file is copied to spec/ when you run 'rails generate rspec:install'
require 'spec_helper'
require File.expand_path('dummy/config/environment', __dir__)
require 'rails'
require 'pry'
require 'faker'
require 'money'
require 'monetize'

require 'simplecov'
SimpleCov.start 'rails' do
  coverage_dir 'tmp/coverage'
  add_group 'Interactors', 'app/interactors'
  add_group 'Services', 'app/services'
  add_group 'Policies', 'app/policies'

  add_filter 'lib/import'
  add_filter 'lib/scripts'
end

ENV['RAILS_ENV'] ||= 'test'
# require File.expand_path('../config/environment', __dir__)
# abort('The Rails environment is running in production mode!') if Rails.env.production?

# require 'rspec/rails'
# require 'pundit/rspec'

# begin
#   ActiveRecord::Migration.maintain_test_schema!
# rescue ActiveRecord::PendingMigrationError => e
#   puts e.to_s.strip
#   exit 1
# end
# RSpec.configure do |config|
#   # config.fixture_path = "#{::Rails.root}/spec/fixtures"

#   # config.use_transactional_fixtures = true

#   # config.infer_spec_type_from_file_location!

#   # config.filter_rails_from_backtrace!

#   # config.include FactoryBot::Syntax::Methods
#   # config.include Devise::Test::IntegrationHelpers, type: :request
# end
