# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Item, type: :model do
  describe 'setting integer as type' do
    let(:number) { Faker::Number.digit }

    context 'when setting a string' do
      let(:item) { described_class.create(amount: number) }

      it { expect(item.amount).to eq(number) }
    end

    context 'when setting an invalid string' do
      let(:item) { described_class.create(amount: Faker::Lorem.word) }

      it { expect(item.amount).to eq(0) }
    end

    context 'when setting an integer' do
      let(:item) { described_class.create(amount: number.to_s) }

      it { expect(item.amount).to eq(number) }
    end
  end

  describe 'setting boolean as type' do
    context 'when setting a boolean' do
      let(:bool) { Faker::Boolean.boolean }
      let(:item) { described_class.create(confirmed: bool) }

      it { expect(item.confirmed).to eq(bool) }
    end

    context 'when setting true/false string' do
      let(:bool) { Faker::Boolean.boolean }
      let(:item) { described_class.create(confirmed: bool.to_s) }

      it { expect(item.confirmed).to eq(bool) }
    end

    context 'when setting 1 string' do
      let(:item) { described_class.create(confirmed: '1') }

      it { expect(item.confirmed).to be_truthy }
    end

    context 'when setting 0 string' do
      let(:item) { described_class.create(confirmed: '0') }

      it { expect(item.confirmed).to be_falsey }
    end

    context 'when setting t string' do
      let(:item) { described_class.create(confirmed: 't') }

      it { expect(item.confirmed).to be_truthy }
    end

    context 'when setting f string' do
      let(:item) { described_class.create(confirmed: 'f') }

      it { expect(item.confirmed).to be_falsey }
    end

    # TODO: review this.
    context 'when setting an invalid string' do
      let(:item) { described_class.create(confirmed: Faker::Lorem.word) }

      it { expect(item.confirmed).to be_truthy }
    end

    context 'when using the attribute? method' do
      let(:bool) { Faker::Boolean.boolean }
      let(:item) { described_class.create(confirmed: bool) }

      it { expect(item.confirmed?).to eq(bool) }
    end
  end

  describe 'setting date as type' do
    context 'when setting a date' do
      let(:date) { Faker::Date.backward }
      let(:item) { described_class.create(paid_at: date) }

      it { expect(item.paid_at).to eq(date) }
      it { expect(item.data['paid_at']).to eq(date.to_s) }
    end

    context 'when setting a string' do
      let(:date) { Faker::Date.backward }
      let(:item) { described_class.create(paid_at: date.to_s) }

      it { expect(item.paid_at).to eq(date) }
      it { expect(item.data['paid_at']).to eq(date.to_s) }
    end

    context 'when setting an invalid string' do
      let(:date) { Faker::Number.digit.to_s }
      let(:item) { described_class.create(paid_at: date.to_s) }

      xit { expect(item.paid_at).to eq(date) }
      xit { expect(item.data['paid_at']).to eq(date.to_s) }
    end
  end

  describe 'setting datetime as type' do
    context 'when setting a datetime' do
      let(:date) { Faker::Time.backward }
      let(:item) { described_class.create(submitted_at: date) }

      it { expect(item.submitted_at).to eq(date) }
      it { expect(item.data['submitted_at']).to eq(date.in_time_zone.to_s) }
    end

    context 'when setting a string' do
      let(:date) { Faker::Time.backward }
      let(:item) { described_class.create(submitted_at: date.strftime('%Y-%m-%d %H:%M:%S')) }

      # TODO: There might be a bug here related to timezones
      xit { expect(item.submitted_at).to eq(date) }
      xit { expect(item.data['submitted_at']).to eq(date.to_s) }
    end

    context 'when setting an invalid string' do
      let(:date) { Faker::Number.digit.to_s }
      let(:item) { described_class.create(submitted_at: date.to_s) }

      xit { expect(item.submitted_at).to eq(date) }
      xit { expect(item.data['submitted_at']).to eq(date.to_s) }
    end
  end

  describe 'setting price as type' do
    context 'when setting a Money' do
      let(:price) { Money.new(Faker::Number.number(digits: 6)) }
      let(:item) { described_class.create(price: price) }

      it { expect(item.price).to eq(price) }
      it { expect(item.data['price_cents']).to eq(price.cents) }
    end

    context 'when setting a string' do
      let(:price) { Money.new(Faker::Number.number(digits: 6)) }
      let(:item) { described_class.create(price: price.to_s) }

      it { expect(item.price).to eq(price) }
      it { expect(item.data['price_cents']).to eq(price.cents) }
    end

    context 'when setting an invalid string (with hyphen)' do
      let(:price) { Money.new(Faker::Number.number(digits: 6)) }
      let(:invalid_price) { "#{price}-#{Faker::Number.digit}" }
      let(:item) { described_class.create(price: invalid_price) }

      it { expect(item.price).to eq(Money.new(invalid_price)) }
      it { expect(item.data['price_cents']).to eq(invalid_price) }

      it do
        item.valid?
        expect(item.errors[:price]).to eq([I18n.t('errors.messages.invalid')])
      end
    end

    context 'when setting an invalid string (with ..)' do
      let(:price) { Faker::Number.number(digits: 5) }
      let(:invalid_price) { "#{price}..#{Faker::Number.digit}" }
      let(:item) { described_class.create(price: invalid_price) }

      it { expect(item.price).to eq(Money.new(0)) }
      it { expect(item.data['price_cents']).to eq(invalid_price) }

      it do
        item.valid?
        expect(item.errors[:price]).to eq([I18n.t('errors.messages.invalid')])
      end
    end
  end

  describe 'setting enum as type' do
    context 'when setting an integer' do
      let(:number) { rand(3) }
      let(:item) { described_class.create(category: number) }

      it { expect(item.category).to eq(described_class.categories.invert[number]) }
      it { expect(item.data['category']).to eq(number) }
    end

    context 'when setting a string' do
      let(:category) { %w[medical income other].sample }
      let(:item) { described_class.create(category: category) }

      it { expect(item.category).to eq(category) }
      it { expect(item.data['category']).to eq(described_class.categories[category]) }
    end

    context 'when setting a symbol' do
      let(:category) { %i[medical income other].sample }
      let(:item) { described_class.create(category: category) }

      it { expect(item.category).to eq(category.to_s) }
      it { expect(item.data['category']).to eq(described_class.categories[category.to_s]) }
    end

    context 'when setting an invalid number' do
      it 'raises error when number is invalid' do
        expect { described_class.create(category: 4) }.to raise_error(ArgumentError)
      end
    end

    context 'when setting an invalid symbol' do
      it 'raises error when number is invalid' do
        expect { described_class.create(category: :invalid) }.to raise_error(ArgumentError)
      end
    end

    context 'when getting the category with ?' do
      let(:item) { described_class.create(category: :medical) }

      it { expect(item).to be_medical }
      it { expect(item).not_to be_income }
      it { expect(item).not_to be_other }
    end
  end

  describe 'setting enum as phone' do
    context 'when setting an integer' do
      let(:phone_number) { Faker::PhoneNumber.phone_number }
      let(:item) { described_class.create(phone: phone_number) }

      it { expect(item.phone).to eq(phone_number) }
      it { expect(item.phone_country).to eq('AU') }
    end
  end
end
